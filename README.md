[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# hosts.lsrules

A small wrapper around [hosts][], to serve them as a subscription for [Little Snitch][].

## Usage
Within Little Snitch, subscribe to <https://hosts-lsrules.azurewebsites.net/api/hosts-lsrules>, or click on one of the links below.  
If you want any extensions, add the query parameter `tags`, which may be any of `fakenews`, `gambling`, `porn`, and `social`.
The extensions are the same as for the original [hosts][] extensions.


### Links

This table is equivalent to the one found on [StevenBlack's hosts files](https://github.com/StevenBlack/hosts/blob/master/readme.md#list-of-all-hosts-file-variants).

You might also find [Host Blocker][] useful as well. It is more finely divided than this.

**Host file recipe** | **Subscribe**
:----------------|:--------------:
[Unified hosts = **(adware + malware)**](https://github.com/StevenBlack/hosts/blob/master/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules)
[Unified hosts **+ fakenews**](https://github.com/StevenBlack/hosts/blob/master/alternates/fakenews/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=fakenews)
[Unified hosts **+ gambling**](https://github.com/StevenBlack/hosts/blob/master/alternates/gambling/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=gambling)
[Unified hosts **+ porn**](https://github.com/StevenBlack/hosts/blob/master/alternates/porn/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=porn)
[Unified hosts **+ social**](https://github.com/StevenBlack/hosts/blob/master/alternates/social/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=social)
[Unified hosts **+ fakenews + gambling**](https://github.com/StevenBlack/hosts/blob/master/alternates/fakenews-gambling/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=fakenews,gambling)
[Unified hosts **+ fakenews + porn**](https://github.com/StevenBlack/hosts/blob/master/alternates/fakenews-porn/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=fakenews,porn)
[Unified hosts **+ fakenews + social**](https://github.com/StevenBlack/hosts/blob/master/alternates/fakenews-social/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=fakenews,social)
[Unified hosts **+ gambling + porn**](https://github.com/StevenBlack/hosts/blob/master/alternates/gambling-porn/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=gambling,porn)
[Unified hosts **+ gambling + social**](https://github.com/StevenBlack/hosts/blob/master/alternates/gambling-social/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=gambling,social)
[Unified hosts **+ porn + social**](https://github.com/StevenBlack/hosts/blob/master/alternates/porn-social/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=porn,social)
[Unified hosts **+ fakenews + gambling + porn**](https://github.com/StevenBlack/hosts/blob/master/alternates/fakenews-gambling-porn/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=fakenews,gambling,porn)
[Unified hosts **+ fakenews + gambling + social**](https://github.com/StevenBlack/hosts/blob/master/alternates/fakenews-gambling-social/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=fakenews,gambling,social)
[Unified hosts **+ fakenews + porn + social**](https://github.com/StevenBlack/hosts/blob/master/alternates/fakenews-porn-social/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=fakenews,porn,social)
[Unified hosts **+ gambling + porn + social**](https://github.com/StevenBlack/hosts/blob/master/alternates/gambling-porn-social/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=gambling,porn,social)
[Unified hosts **+ fakenews + gambling + porn + social**](https://github.com/StevenBlack/hosts/blob/master/alternates/fakenews-gambling-porn-social/readme.md) | [Subscribe](x-littlesnitch:subscribe-rules?url=https%3A%2F%2Fhosts-lsrules.azurewebsites.net%2Fapi%2Fhosts-lsrules?tags=fakenews,gambling,porn,social)


[hosts]: https://github.com/StevenBlack/hosts
[Little Snitch]: https://obdev.at/products/littlesnitch/index.html
[host blocker]: https://hostblocker.app
