import logging
from typing import Set, Optional, List, Iterable
from requests import request
import azure.functions as func


class InvalidTagError(TypeError):
    pass


VALID_TAGS = {"hosts", "fakenews", "gambling", "porn", "social"}


def get_tags(req: func.HttpRequest) -> Set[str]:
    tags = req.params.get("tags")
    if not tags:
        try:
            req_body = req.get_json()
        except ValueError:
            pass
        else:
            tags = req_body.get("tags")

    if tags is None:
        return set()
    return set([tag.strip() for tag in tags.split(",")])


def get_lsrules(tags: Optional[Set[str]] = None) -> dict:
    if tags is None:
        tags = set()

    url = get_link(tags)
    domains = get_domains(url)
    return {
        "name": f"StevenBlack/hosts, with extensions",
        "description": f"Blocks access to {', '.join(_sort_tags(tags))}",
        "denied-remote-domains": domains,
    }


def hosts(tags: Set[str]):
    if tags > VALID_TAGS:
        raise InvalidTagError(
            f"Invalid tag(s) found: {tags - VALID_TAGS}. The supported tags are {VALID_TAGS}"
        )
    return get_lsrules(tags)


def get_domains(url: str) -> List[str]:
    hosts = request("GET", url).text
    domains = []
    for line in hosts.split("\n"):
        if not line or line.startswith("#") or not line.startswith("0.0.0.0"):
            # Ignore all irrelevant lines
            continue
        _, domain, *comment = line.split(" ")
        if domain == "0.0.0.0":
            continue
        domains.append(domain)
    return domains


def _sort_tags(tags: Iterable[str]) -> List[str]:
    return sorted(list(tags), reverse=False)


def get_link(tags: Set[str]) -> Optional[str]:
    suffix = "hosts"
    if tags:
        tags = _sort_tags(tags)
        suffix = f"alternates/{'-'.join(tags)}/{suffix}"
        logging.info(suffix)
    return f"https://raw.githubusercontent.com/StevenBlack/hosts/master/{suffix}"
