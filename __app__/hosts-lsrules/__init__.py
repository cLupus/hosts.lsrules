import json
import logging
import azure.functions as func

from .app import hosts, InvalidTagError, get_tags


def main(req: func.HttpRequest) -> func.HttpResponse:
    tags = get_tags(req)
    logging.info(f"Requested the tags: {', '.join(tags)}")
    try:
        return func.HttpResponse(
            json.dumps(hosts(tags)), mimetype="application/json", charset="utf-8",
        )
    except InvalidTagError as e:
        return func.HttpResponse(e.args, status_code=400)
